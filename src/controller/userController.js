const db = require("../../models")
const User = db.User
const Profile = db.Profile
const Review = db.Review
const Job_Category = db.Job_Category
const Skill = db.Skill
const Currency = db.Currency
const Job_Post = db.Job_Post
const Time_Zone = db.Time_Zone
const { Op } = require("sequelize")
const joi = require("joi")


async function completeprofile(req, res) {
  try {
    const profile = joi.object({
      first_name: joi.string(),
      last_name: joi.string(),
      city: joi.string(),
      country: joi.string(),
      profile_description: joi.string().min(100).max(500),
      github_link: joi.string(),
      linkedin_link: joi.string(),
      portfolio_site_link: joi.string(),
      skills: joi.array().items(joi.string()),
      education: joi.string(),
      hourly_rate: joi.number().integer(),
      add_tagline: joi.string()
    })

    const { error } = profile.validate(req.body)

    if (error) {
      return res.send({ Error: error.details[0].message })
    }

    const {
      first_name,
      last_name,
      city,
      country,
      profile_description,
      github_link,
      linkedin_link,
      portfolio_site_link,
      skills,
      education,
      hourly_rate,
      add_tagline
    } = req.body

    user_id = req.user.id
    s = skills.join(",")

    user = await User.findOne({
      where: { id: user_id }
    })


    if (user.dataValues.is_complete == true) {
      return res.status(200).send({ message: "profile is already update" })
    }


    const c_name = await Time_Zone.findOne({
      where: { country_name: country }
    })

   if(!c_name)
   {  
    return res.status(422).send({ message: 'country not found' })
   }


   await Profile.create({
      first_name,
      last_name,
      city,
      country,
      time_zone: c_name.dataValues.time_zone,
      profile_description,
      github_link,
      linkedin_link,
      portfolio_site_link,
      skills: s,
      education,
      hourly_rate,
      add_tagline,
      user_id
    })

 await User.update(
      { is_complete: true },
      {
        where: {
          id: user_id
        }
      }
    )

    return res.status(200).send({ Message: "user profile updated", is_complete: true })

  }
   catch (e) {
    return res.status(422).send({ Error: e })
  }
}



async function profilepic(req, res) {
  try {

    user_id = req.user.id

    if (req.file) {
      path = req.file.path


      await Profile.update(
        { image: path },
        {
          where: {
            user_id: user_id
          }
        }
      )
    }
    return res.status(200).send({ Message: "profile pic updated" })

  } catch (e) {
    console.log(e)
    return res.status(422).send({ Error: e })
  }
}


async function reviews(req, res) {
  try {

    const reviews = joi.object({

      delivered_on_budget:joi.boolean(),
      delivered_on_time:joi.boolean(),
      text_reviews: joi.string(),
      star_reviews: joi.number().integer()
    })

    const { error } = reviews.validate(req.body)

    if (error) {
      return res.status(422).send({ Error: error.details[0].message })
    }

    const {
      delivered_on_budget,
      delivered_on_time,
      text_reviews,
      star_reviews
    } = req.body

    user_id_review_given_to = req.params.id
    user_id_review_given_by = req.user.id

    await User.findOne({
      where: { id: user_id_review_given_by }
    })

    users = await User.findOne({
      where: { id: user_id_review_given_to }
    })

    if (!users) {
      return res.status(422).send({ Message: "User not found" })
    }

    const review = await Review.create({
      delivered_on_budget,
      delivered_on_time,
      text_reviews,
      star_reviews,
      user_id_review_given_to,
      user_id_review_given_by
    })

    return res.status(200).send({ message: review })
  }
  catch (e) {
    return res.status(422).send({ Error: e })
  }
}

async function getreviews(req, res) {
  try {

    user_id = req.user.id
    const rating = await Review.findAll({
      include: [
        {
          model: User,
          attributes: ['unique_name']
        }
      ],
      where: { user_id_review_given_by: user_id }
    })
    return res.status(200).send({ "Reviews": rating })
  } catch (e) {
    return res.status(422).send({ Error: e })
  }
}


async function getprofile(req, res) {
  try {
    user_id = req.user.id
    const profile = await User.findOne({
      attributes: ["unique_name", "email", "is_complete", "role"],
      include: [
        {
          model: Profile,
          attributes: ["first_name", "last_name", "image", "city", "country", "time_zone", "profile_description", "github_link", "linkedin_link", "portfolio_site_link", "skills", "education", "hourly_rate", "add_tagline"]
        },
        {
          model: Review,
          attributes: ["delivered_on_budget","delivered_on_time","text_reviews", "star_reviews", "user_id_review_given_to", "user_id_review_given_by"]
        }
      ],
      where: { id: user_id }
    })


    var len = (profile.dataValues.Reviews).length
    sum = 0
    for (i = 0; i < len; i++) {
      sum = sum + profile.dataValues.Reviews[i].star_reviews
    }
    let average = sum / len

    return res.status(200).send({ "Userprofile": profile, 'averageReview': average })
  } catch (e) {
    return res.status(422).send({ Error: e })
  }
}


async function getjobcategory(req, res) {
  try {

    user_id = req.user.id

    const job_category = await Job_Category.findAll({})

    return res.status(200).send({ "JobCategory": job_category })
  }
  catch (e) {
    return res.status(422).send({ Error: e })
  }
}


async function getskill(req, res) {
  try {
    user_id = req.user.id
    const skill = await Skill.findAll({})
    return res.status(200).send({ "Skill": skill })
  }
  catch (e) {
    return res.status(422).send({ Error: e })
  }

}


async function getcurrency(req, res) {
  try {
    user_id = req.user.id
    const currency = await Currency.findAll({})
    return res.status(200).send({ "Currency": currency })
  }
  catch (e) {
    return res.status(422).send({ Error: e })
  }
}


async function searchfreelancerbyname(req, res) {
  try {
    const { name } = req.body
    const u = await User.findOne({
      attributes: ["id", "unique_name", "role"],
      where: { unique_name: { [Op.like]: '%' + name + '%' } }
    })
    console.log(u.dataValues.role)
    if (u.dataValues.role !== "freelancer") {
      return res.status(422).send({ message: "User not found" })
    }
    else {
      return res.status(200).send({ User: u })
    }
  }
  catch (e) {
    console.log(e)
    return res.status(422).send({ Error: e })
  }
}


async function searchfreelancerbyskills(req, res) {

  const { skill } = req.body
  // s = skill.join(",") 

  const u = await User.findAll({
    attributes: ["unique_name", "role"],
    include: [
      {
        model: Profile,
        attributes: ["first_name", "last_name", "skills"]

      }
    ],
    where:
    {
      role: "freelancer"
    }
  })

  // console.log(u[0].dataValues.Profile.dataValues.skills)

  len = u.length
  us = ''
  for (i = 0; i < len; i++) {

    us = us + u[i].Profile.dataValues.skills
  }
  console.log(us)
  

  return res.status(200).send({ 'data': u })
}



async function searchjobbyid(req, res) {
  try {

    const { job_id } = req.body

    job_post = await Job_Post.findOne(
      {
        where: { id: job_id },

        attributes: [
          "id", "job_description",
          "user_id",
          "budget",
          "category",
          "preferred_currency_type",
          "currency_type",
          "cover_letter",
          "preferred_skills",
          "location",
          "duration",
          "no_of_offers"
        ]
      }
    )

    return res.status(200).send({ Job_Post: job_post })
  } catch (e) {
    return res.status(422).send({ Error: e })
  }

}


async function updateprofile(req, res) {
  try {

    const updateprofile = joi.object({

      first_name: joi.string(),
      last_name: joi.string(),
      city: joi.string(),
      country: joi.string(),
      profile_description: joi.string().min(100).max(500),
      github_link: joi.string(),
      linkedin_link: joi.string(),
      portfolio_site_link: joi.string(),
      skills: joi.array().items(joi.string()),
      education: joi.string(),
      hourly_rate: joi.number().integer(),
      add_tagline: joi.string()
    })

    const { error } = updateprofile.validate(req.body)
    if (error) {
      return res.send({ Error: error.details[0].message })
    }

    const {
      first_name,
      last_name,
      city,
      country,
      profile_description,
      github_link,
      linkedin_link,
      portfolio_site_link,
      skills,
      education,
      hourly_rate,
      add_tagline
    } = req.body

    s = skills.join(",")
    user_id = req.user.id


    const c_name = await Time_Zone.findOne({
      where: { country_name: country }
    })

   if(!c_name)
   {  
    return res.status(422).send({ message: 'country not found' })
   }

    await Profile.update({
      first_name,
      last_name,
      city,
      country,
      time_zone: c_name.dataValues.time_zone,
      profile_description,
      github_link,
      linkedin_link,
      portfolio_site_link,
      skills: s,
      education,
      hourly_rate,
      add_tagline
    },
      {
        where: { id: user_id }
      })
    return res.status(200).send({ message: 'profile updated succesfully' })

  } catch (e) {
    return res.status(422).send({ error: e })
  }
}




module.exports = {
  completeprofile,
  profilepic,
  reviews,
  getprofile,
  getreviews,
  getjobcategory,
  getskill,
  getcurrency,
  searchfreelancerbyname,
  searchfreelancerbyskills,
  searchjobbyid,
  updateprofile
}

