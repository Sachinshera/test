const db = require("../../models")
const Job_Category = db.Job_Category
const Skill = db.Skill
const Currency = db.Currency
const User = db.User
const Profile = db.Profile
const { Op } = require("sequelize")
const bcrypt = require("bcrypt")
const joi = require("joi")

async function addjobcategory(req, res) {
  try {
    const jobcategory = joi.object({
      category_type: joi.string().required()
    })
    
    const { error } = jobcategory.validate(req.body)

    if (error) {
      return res.status(422).send({ Error: error.details[0].message })
    }
    const { category_type } = req.body
    
    admin_id = req.user.id

    const admin = await User.findOne({
      where: { id: admin_id }
    })

    if (admin.role !== "admin") {
      return res.status(422).send({ message: "Invalid Operation" })
    }

    await Job_Category.create({
      category_type
    })

    return res.status(200).send({ message: "Add Job Category successfully" })
  }
  catch (e) {
    return res.status(422).send({ error: e })
  }
}


async function addskill(req, res) {
  try {

    const skill = joi.object({
      skill_type: joi.string().required()
    })

    const { error } = skill.validate(req.body)

    if (error) {
      return res.status(422).send({ Error: error.details[0].message })
    }

    const { skill_type } = req.body
    admin_id = req.user.id

    const admin = await User.findOne({
      where: { id: admin_id }
    })

    if (admin.role !== "admin") {
      return res.status(422).send({ message: "Invalid Operation" })
    }

    await Skill.create({
      skill_type
    })

    return res.status(200).send({ message: "Add Skill successfully" })
  }
  catch (e) {
    return res.status(422).send({ error: e })
  }
}


async function addcurrency(req, res) {
  try {

    const currency = joi.object({
      currency_type: joi.string().required(),
      user_address: joi.string().required(),
      currency_decimals: joi.number()
    })

    const { error } = currency.validate(req.body)

    if (error) {
      return res.status(422).send({ Error: error.details[0].message })
    }

    const {
      currency_type,
      user_address,
      currency_decimals
    } = req.body

    admin_id = req.user.id

    const admin = await User.findOne({
      where: { id: admin_id }
    })

    if (admin.role !== "admin") {
      return res.status(422).send({ message: "Invalid Operation" })
    }

    await Currency.create({
      currency_type,
      user_address,
      currency_decimals
    })

    return res.status(200).send({ message: "Add Currency successfully" })
  }
  catch (e) {
    console.log(e)
    return res.status(422).send({ error: e })
  }
}


async function showclient(req, res) {

  try {
    admin_id = req.user.id

    const admin = await User.findOne({
      where: { id: admin_id }
    })

    if (admin.role !== "admin") {
      return res.status(422).send({ message: "Invalid Operation" })
    }

    const client = await User.findAndCountAll({
      attributes: ["id", "unique_name", "role"],
      where: { role: "client" }
    })

    return res.status(200).send({ Client: client })
  }
  catch (e) {
    return res.status(422).send({ error: e })
  }
}


async function showfreelancer(req, res) {
  try {
    admin_id = req.user.id
    const admin = await User.findOne({
      where: { id: admin_id }
    })

    if (admin.role !== "admin") {
      return res.status(422).send({ message: "Invalid Operation" })
    }

    const freelancer = await User.findAndCountAll({
      attributes: ["id", "unique_name", "role"],
      where: { role: "freelancer" }
    })
    return res.status(200).send({ Freelancer: freelancer })
  }
  catch (e) {
    return res.status(422).send({ error: e })
  }
}


async function showboth(req, res) {

  try {

    admin_id = req.user.id

    const admin = await User.findOne({
      where: { id: admin_id }
    })

    if (admin.role !== "admin") {
      return res.status(422).send({ message: "Invalid Operation" })
    }

    const data = await User.findAndCountAll({

      attributes: ["id", "unique_name", "role"],
      where: {
        [Op.or]: [
          { role: "freelancer" },
          { role: "client" }
        ]
      },
      order: [
        ['id', 'DESC']
      ]
    })
    return res.status(200).send({ Data: data })
  }
  catch (e) {
    return res.status(422).send({ error: e })
  }
}


async function updateuser(req, res) {
  try {

    const update = joi.object({

      first_name: joi.string(),
      last_name: joi.string(),
      city: joi.string(),
      country: joi.string(),
      profile_description: joi.string().min(100).max(500),
      github_link: joi.string(),
      linkedin_link: joi.string(),
      portfolio_site_link: joi.string(),
      skills: joi.array().items(joi.string()),
      education: joi.string(),
      hourly_rate: joi.number().integer(),
      add_tagline: joi.string(),
      role: joi.string()
    })

    const { error } = update.validate(req.body)
    if (error) {
      return res.send({ Error: error.details[0].message })
    }

    const {
      first_name,
      last_name,
      city,
      country,
      profile_description,
      github_link,
      linkedin_link,
      portfolio_site_link,
      skills,
      education,
      hourly_rate,
      add_tagline,
      role
    } = req.body

    s = skills.join(",")

    user_id = req.params.id
    admin_id = req.user.id

    const admin = await User.findOne({
      where: { id: admin_id }
    })

    if (admin.role !== "admin") {
      return res.status(422).send({ message: "Invalid Operation" })
    }

    await Profile.update(
      {
        first_name,
        last_name,
        city,
        country,
        profile_description,
        github_link,
        linkedin_link,
        portfolio_site_link,
        skills: s,
        education,
        hourly_rate,
        add_tagline
      },
      {
        where: { user_id: user_id }
      }
    )

    await User.update(
      { role: role },
      {
        where: { id: user_id }
      }
    )
    return res.status(200).send({ Message: "User Updated Successfully" })
  }
  catch (e) {
    return res.status(422).send({ error: e })
  }
}


async function blockuser(req, res) {
  try {

    user_id = req.params.id
    admin_id = req.user.id

    const admin = await User.findOne({
      where: { id: admin_id }
    })

    if (admin.role !== "admin") {
      return res.status(422).send({ Message: "Invalid Operation" })
    }

    await User.update(
      { is_block: true },
      {
        where: { id: user_id }
      }
    )

    return res.status(200).send({ Message: "User is blocked" })
  }
  catch (e) {
    return res.status(422).send({ error: e })
  }
}


async function deleteuser(req, res) {
  try {

    user_id = req.params.id
    admin_id = req.user.id

    const admin = await User.findOne({
      where: { id: admin_id }
    })

    if (admin.role !== "admin") {
      return res.status(422).send({ message: "Invalid Operation" })
    }

    await User.destroy({
      where: { id: user_id }
    })

    return res.status(200).send({ Message: "User Deleted Successfully" })
  }
  catch (e) {
    return res.status(422).send({ error: e })
  }
}


async function unblockuser(req, res) {

  try {

    user_id = req.params.id
    admin_id = req.user.id

    const admin = await User.findOne({
      where: { id: admin_id }
    })

    if (admin.role !== "admin") {
      return res.status(422).send({ Message: "Invalid Operation" })
    }

    await User.update(
      { is_block: false },
      {
        where: { id: user_id }
      }
    )

    return res.status(200).send({ Message: "User is unblocked" })
  }
  catch (e) {
    return res.status(422).send({ error: e })
  }

}

async function adduser(req, res) {
  try {

    const adduser = joi.object({

      email: joi.string().email().required(),
      role: joi.string().required(),
      password: joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{8,30}$')).required(),
      unique_name: joi.string().required()
    })

    const { error } = adduser.validate(req.body)
    if (error) {
      return res.send({ Error: error.details[0].message })
    }

    const { unique_name, email, role, password } = req.body

    admin_id = req.user.id

    const admin = await User.findOne({
      where: { id: admin_id }
    })

    if (admin.role !== "admin") {
      return res.status(422).send({ Message: "Invalid Operation" })
    }

    const user_name = await User.findOne({
      where: { unique_name: unique_name }
    })

    if (user_name) {
      return res.status(422).send({ Message: "User is already register" })
    }


    const user_email = await User.findOne({
      where: { email: email }
    })

    if (user_email) {
      return res.status(422).send({ Message: "User is already register" })
    }

    const salt = bcrypt.genSaltSync(10)
    const hash = bcrypt.hashSync(password, salt)

    await User.create({
      unique_name,
      email,
      role,
      password: hash,
      is_verified: true,
      is_admin: false
    })

    return res.status(200).send({ Message: "User added Successfully" })
  }
  catch (e) {
    return res.status(422).send({ error: e })
  }
}

module.exports = {
  addjobcategory,
  addskill,
  addcurrency,
  showclient,
  showfreelancer,
  showboth,
  updateuser,
  blockuser,
  deleteuser,
  unblockuser,
  adduser
}
