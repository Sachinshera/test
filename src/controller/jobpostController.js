const db = require("../../models")
const Job_Post = db.Job_Post
const User = db.User
const Proposal = db.Proposal
const joi = require("joi")


async function jobpost(req, res) {
    try {

        const jobpost = joi.object({
            job_description: joi.string(),
            budget: joi.number().integer(),
            category: joi.string(),
            preferred_currency_type: joi.string(),
            currency_type: joi.string(),
            cover_letter: joi.string(),
            preferred_skills: joi.string(),
            location: joi.string(),
            duration: joi.string(),
            no_of_offers: joi.number().integer()

        })

        const { error } = jobpost.validate(req.body)

        if (error) {
            return res.status(422).send({ Error: error.details[0].message })
        }

        user_id = req.user.id

        const {
            job_description,
            budget,
            category,
            preferred_currency_type,
            currency_type,
            cover_letter,
            preferred_skills,
            location,
            duration,
            no_of_offers
        } = req.body

        const u = await User.findOne({
            where: { id: user_id }
        })
        if (u.role !== "client") {
            return res.status(422).send({ message: "Freelancer Cannot Post Job" })
        }
        else {
            const job_post = await Job_Post.create({

                job_description,
                budget,
                category,
                preferred_currency_type,
                currency_type,
                cover_letter,
                preferred_skills,
                location,
                duration,
                no_of_offers,
                user_id_job_posted_by: user_id
            })
            return res.status(200).send({ Data: job_post })
        }

    } catch (e) {
        return res.status(422).send({ Error: e })
    }
}


async function proposals(req, res) {
    try {

        const pro = joi.object({

            job_post_id: joi.number().integer(),
            proposal_price: joi.number().integer(),
            preferred_currency_type: joi.string(),
            proposal: joi.string(),
            eth_address: joi.string()

        })

        const { error } = pro.validate(req.body)

        if (error) {
            return res.status(422).send({ Error: error.details[0].message })
        }


        user_id = req.user.id

        const {
            job_post_id,
            proposal_price,
            preferred_currency_type,
            proposal,
            eth_address
        } = req.body

        const us = await User.findOne({
            where: { id: user_id }

        })

        if (us.role !== "freelancer") {
            return res.status(422).send({ Message: "Client Cannot post proposal" })
        }
        else {
            const proposals = await Proposal.create({
                job_post_id,
                proposal_price,
                preferred_currency_type,
                proposal,
                eth_address,
                user_id_freelancer: user_id
            })
            return res.status(200).send({ Data: proposals })
        }
    }
    catch (e) {
        return res.status(422).send({ Error: e })
    }
}


async function getproposalspost(req, res) {

    try {
        const { id } = req.body

        const proposals = await Proposal.findAll({
            attributes: ['job_post_id', 'user_id_freelancer', 'proposal_price', 'preferred_currency_type', 'proposal', 'eth_address'],
            include: [
                {
                    model: User,
                    attributes: ["unique_name"]
                }
            ],
            where: { job_post_id: id }
        })
        return res.status(200).send({ "Proposal": proposals })
    }
    catch (e) {
        return res.status(422).send({ Error: e })
    }
}



  module.exports = {
       jobpost,
       proposals,
       getproposalspost,
     }


