const { Sequelize } = require("sequelize")
const db = require("../../models")
const User = db.User
const Refresh_Token = db.Refresh_Token
const joi = require("joi")
const bcrypt = require("bcrypt")
const jwt = require("jsonwebtoken")
var otpGenerator = require('otp-generator')
const sgMail = require('@sendgrid/mail')
otp = null
login_otp = null
uss = null
u = null


async function verifymail(req, res) {

  try {
    const signup = joi.object({
      email: joi.string().email().required()
    })

    const { error } = signup.validate(req.body)

    if (error) {
      return res.status(422).send({ Error: error.details[0].message })
    }

    let { email } = req.body

    const u = await User.findOne({
      where: { email }
    })

    if (u != null) {
      return res.status(422).send({ message: 'user is already register' })
    }
    return res.status(200).send({ message: "email is available" })

  } catch (e) {
    return res.status(422).send({ error: e })
  }
}


async function verifyname(req, res) {

  try {
    const signup = joi.object({
      names: joi.string().required()
    })

    const { error } = signup.validate(req.body)

    if (error) {

      return res.status(422).send({ Error: error.details[0].message })
    }

    let { names } = req.body

    const n = await User.findOne({
      where: { unique_name: names }
    })

    if (n != null) {
      return res.status(422).send({ message: 'user is already register' })
    }

    return res.status(200).send({ message: "name is available" })

  } catch (e) {
    return res.status(422).send({ error: e })
  }
}


async function signup(req, res) {
  try {

    const reg = joi.object({
      first_name: joi.string().required(),
      last_name: joi.string().required(),
      password: joi.string()
      .pattern(new RegExp('^[a-zA-Z0-9]{8,30}$')).required(),
      confirm_password: joi.ref('password'),
      email: joi.string().email().required(),
      role: joi.number().required(),
      city: joi.string().required(),
      send_useful_emails: joi.boolean(),
      terms_of_use: joi.boolean(),
    })
    const { error } = reg.validate(req.body)
    if (error) {
      return res.send({ Error: error.details[0].message })
    }

    const { first_name, last_name, password, confirm_password, email, role, city, send_useful_emails, terms_of_use } = req.body

    const find_user = await User.findOne({
      where: [{
        email
      }]
    })
    if (find_user) {
      return res.status(422).send({ message: 'User is already register' })
    }

    const salt = bcrypt.genSaltSync(10) 
    const hash = bcrypt.hashSync(password, salt) 

    const user=await User.create({
      first_name,
      last_name,
      password: hash,
      email,
      role,  
      city,
      send_useful_emails,
      terms_of_use,
      is_verified: false
    })

   values=user.dataValues 

    const access_token = jwt.sign({ first_name, last_name, password, email, role, city, send_useful_emails, terms_of_use }, process.env.JWT_SECRET)
    

    const verify_url = `${process.env.URL}/api/activate/${access_token}`
    sgMail.setApiKey(process.env.SENDGRID_API_KEY)
    const msgs = {
      to: email,
      from: 'dipali.brinfotech@gmail.com',
      subject: 'Dlance Registration..',
      text: 'Email Register',
      html: `<a href="${verify_url}">Click Here to verify account</a>`
    }
    sgMail
      .send(msgs)
      .then((result) => {
        console.log(result)
        return res.status(200).send({ message: 'email has been sent please verify your account' })
      })

  } catch (e) {
    console.log(e)
    return res.status(422).send({ message: e })
  }

}


async function activate(req, res) {
  try {

    const { access_token } = req.params
    console.log(access_token)
    const access_to = await jwt.verify(access_token, process.env.JWT_SECRET)
   

    const { email } = access_to

      await User.update({ is_verified: true }, {
       where: {
        email: email
      }
    });

    return res.status(200).send({
      message: 'succesfully verified'
    })
  } catch (e) {
    console.log(e)
    return res.status(422).send({ error: e })
  }
}


async function login(req, res) {
  try {
    let login = joi.object({
      email: joi.string().email().required(),
      password: joi
        .string()
        .pattern(new RegExp("^[a-zA-Z0-9]{8,30}$"))
        .required()
    })
    const { error } = login.validate(req.body)
    if (error) {
      return res.status(422).send({ Error: error.details[0].message })
    }
    let { email, password } = req.body
    u = await User.findOne({
      where: { email }
    })

    if (!u) {
      return res.status(200).send({ Message: "user not found" })
    }
    const compare = bcrypt.compareSync(password, u.password)

    if (!compare) {
      return res.status(422).send({ Message: 'please enter valid credentials' })
    }

    if (u.is_block === true) {
      return res.status(422).send({ Message: "user is blocked" })
    }

    if (u.is_verified === false) {
      return res.status(422).send({ Message: "Please Verified Your Account" })
    }

    login_otp = otpGenerator.generate(6, { upperCase: false, specialChars: false, alphabets: false, digits: true })
    console.log(login_otp)
    sgMail.setApiKey(process.env.SENDGRID_API_KEY)
    const msgs = {
      to: email,
      from: 'dipali.brinfotech@gmail.com',
      subject: 'Sending Email Registration..',
      text: 'Email Register succesfully',
      html: "<h2> Your Email is  " + email + " \n and Your Otp Is " + login_otp + "</h2>"
    }

    sgMail
      .send(msgs)
      .then(() => {
        return res.json({ message: 'Email sent' })
      })
      .catch((error) => {
        console.log(error)
        return res.json({ message: 'sending Failed' })
      })
  } catch (e) {
    return res.status(422).send({ error: e })
  }
}


async function twofactor(req, res) {
  try {

    let twofactor = joi.object({
      user_otp: joi.string().required()
    })

    const { error } = twofactor.validate(req.body)
    if (error) {
      return res.status(422).send({ Error: error.details[0].message })
    }

    if (!u) {
      return res.json({ message: 'user not found' })
    }

    const { user_otp } = req.body

    if (login_otp != user_otp) {
      return res.status(200).send({ message: "Invalid Otp" })
    }
    else {

      const access_token = jwt.sign({ id: u.id, is_admin: u.is_admin, is_complete: u.is_complete, role: u.role }, process.env.JWT_SECRET, { expiresIn: '15m' })
      const refresh_token = jwt.sign({ id: u.id, is_admin: u.is_admin, is_complete: u.is_complete, role: u.role }, process.env.REFRESH_SECRET, { expiresIn: '1y' })

      await Refresh_Token.create({
        token: refresh_token
      })
      return res.status(200).send({ access_token, refresh_token })
    }
  } catch (e) {
    console.log(e)
    return res.send({ error: e })
  }

}


async function forget(req, res) {
  try {

    let forget = joi.object({
      email: joi.string().email().required()
    })
    const { error } = forget.validate(req.body)
    if (error) {
      return res.status(422).send({ Error: error.details[0].message })
    }
    let { email } = req.body

    uss = await User.findOne({
      where: { email: email }
    })

    if (!uss) {
      return res.json({ message: 'Email not found' })
    }

    const access_token = jwt.sign({ id: uss.id, email: uss.email }, process.env.JWT_RESET_PASSWORD_SECRET, { expiresIn: '30m' })

    const verify_url = `${process.env.URL}/api/change-password/${access_token}`
    sgMail.setApiKey(process.env.SENDGRID_API_KEY)
    const msg = {
      to: email,
      from: 'dipali.brinfotech@gmail.com',
      subject: 'Dlance Registration..',
      text: 'Email Register..',
      html: `<a href="${verify_url}">Click Here to reset your password </a>`

    }

    sgMail
      .send(msg)
      .then(() => {
        return res.json({ message: 'email has been sent please reset your password' })
      })
      .catch((error) => {
        return res.json({ message: 'sending Failed' })
      })

  } catch (e) {
    console.log(e)
    return res.status(422).send({ error: e })
  }
}


async function changepassword(req, res) {
  try {
    let changepassword = joi.object({
      new_password: joi
        .string()
        .pattern(new RegExp("^[a-zA-Z0-9]{8,30}$"))
        .required()
    })
    const { error } = changepassword.validate(req.body)
    if (error) {
      return res.status(200).send({ Error: error.details[0].message })
    }

    const { access_tokn } = req.params

    let User_id

    try {
      const decoded = await jwt.verify(access_tokn, process.env.JWT_RESET_PASSWORD_SECRET, { expiresIn: '30m' })
      User_id = decoded.id
    }
    catch (e) {
      return res.status(422).send({ Message: "invalid token" })
    }

    let { new_password } = req.body

    const salt = bcrypt.genSaltSync(10)
    const hash = bcrypt.hashSync(new_password, salt)

    await User.update({ password: hash }, {
      where: {
        id: User_id
      }
    })

    return res.json({ message: 'password change successfully' })
  }
  catch (e) {
    return res.status(422).send({ error: e })
  }
}


async function refreshtoken(req, res) {

  const ref = joi.object({
    ref_token: joi.string().required()
  })

  const { error } = ref.validate(req.body)

  if (error) {
    return res.status(422).send({ Error: error.details[0].message })
  }

  const { ref_token } = req.body

  try {

    const refresh = await Refresh_Token.findOne({
      where: { token: ref_token }
    })

    if (!refresh) {
      return res.status(422).send({ Message: "invalid refresh token" })
    }

    let user_Id

    try {

      const decoded = jwt.verify(refresh.token, process.env.REFRESH_SECRET)

      user_Id = decoded.id

    } catch (e) {
      return res.status(422).send({ Message: "invalid refresh token" })
    }

    const user = await User.findOne({ where: { id: user_Id } })

    if (!user) {

      return res.send({ Message: "user not found" })

    }

    const access_token = jwt.sign({ id: user.id, is_admin: user.is_admin, is_complete: user.is_complete, role: user.role }, process.env.JWT_SECRET, { expiresIn: '15m' })
    const refresh_token = jwt.sign({ id: user.id, is_admin: user.is_admin, is_complete: user.is_complete, role: user.role }, process.env.REFRESH_SECRET, { expiresIn: '1y' })

    return res.status(200).send({ access_token, refresh_token })

  }
  catch (e) {
    return res.status(422).send({ Error: e })
  }

}


async function logout(req, res, next) {
  try {

    const logout = joi.object({
      refresh_token: joi.string().required()
    })

    const { error } = logout.validate(req.body)

    if (error) {
      return res.status(422).send({ Error: error.details[0].message })
    }

    const { refresh_token } = req.body

    await Refresh_Token.destroy(
      {
        where: { token: refresh_token }
      })

    return res.status(200).send({ message: "User Logout Successfully" })
  }
  catch (e) {
    return res.status(422).send({ error: e })
  }

}

module.exports = {
  verifymail,
  verifyname,
  signup,
  activate,
  login,
  twofactor,
  forget,
  changepassword,
  refreshtoken,
  logout
}


