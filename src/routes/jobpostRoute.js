const express = require('express')
const router = express.Router()
const jobpostController = require('../controller/jobpostController')


router.post('/jobpost', jobpostController.jobpost)
router.post('/proposal', jobpostController.proposals)
router.get('/getproposalpost', jobpostController.getproposalspost)


module.exports = router