const express = require('express')
const router = express.Router()
const authController = require('../controller/authController')
const auth = require('../../middlewares/auth')

router.post('/verifyemail', authController.verifymail)
router.post('/verifyname', authController.verifyname)
router.post('/signup', authController.signup)
router.get('/activate/:access_token', authController.activate)
router.post('/login', authController.login)
router.post('/two-factor', authController.twofactor)
router.post('/forget', authController.forget)
router.put('/change-password/:access_tokn', authController.changepassword)
router.post('/refresh', authController.refreshtoken)
router.delete('/logout', auth, authController.logout)


module.exports = router

