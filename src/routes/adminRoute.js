const express = require('express')
const router = express.Router()
const adminController = require('../controller/adminController')

router.post('/add-job-category', adminController.addjobcategory)
router.post('/add-skill', adminController.addskill)
router.post('/add-currency', adminController.addcurrency)
router.get('/show-client', adminController.showclient)
router.get('/show-freelancer', adminController.showfreelancer)
router.get('/show-both', adminController.showboth)
router.put('/update-user/:id', adminController.updateuser)
router.post('/block-user/:id', adminController.blockuser)
router.delete('/delete-user/:id', adminController.deleteuser)
router.put('/unblock-user/:id', adminController.unblockuser)
router.post('/add-user', adminController.adduser)


module.exports = router