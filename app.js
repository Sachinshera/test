require("dotenv").config({ path: "./config/.env" }) 
const express = require("express") 
const indexRoute = require("./src/routes/indexRoute") 
const httpErrors=require('http-errors')
const cors=require('cors')

const app = express() 
app.use(cors())
app.use(express.json()) 
app.use(express.urlencoded({
    extended: true,
  })
) 

app.get('/',(req,res)=>{
  res.send("Hello DeLance")
})

app.use(indexRoute) 

app.use((req, res, next) => {
   return next(
    httpErrors(404, 'Page Not Found')) 
    next()
}) 

port = process.env.PORT || 8000 

app.listen(port, () => {
  console.log(`Server is listen on port ${port}`) 
}) 