const {
  Model
} = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class Profile extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Profile.init({
    first_name: DataTypes.STRING,
    last_name: DataTypes.STRING,
    image: DataTypes.STRING,
    city: DataTypes.STRING,
    country: DataTypes.STRING,
    time_zone: DataTypes.ARRAY(DataTypes.STRING),
    profile_description: DataTypes.STRING,
    github_link: DataTypes.STRING,
    linkedin_link: DataTypes.STRING,
    portfolio_site_link: DataTypes.STRING,
    skills: DataTypes.STRING,
    education: DataTypes.STRING,
    hourly_rate: DataTypes.INTEGER,
    user_id: DataTypes.INTEGER,
    add_tagline: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Profile'
  })
  Profile.associate = function (models) {
    Profile.belongsTo(models.User, { foreignKey: 'user_id' })
  }
  return Profile
}